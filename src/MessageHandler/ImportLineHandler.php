<?php

namespace App\MessageHandler;

use App\Document\Import;
use App\Message\ImportLine;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ImportLineHandler implements MessageHandlerInterface
{
    private $dm;
    
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }
    
    public function __invoke(ImportLine $importLine)
    {
        if (rand(0, 10) < 7) {
            throw new \Exception('Random exception');
        }
        
        $import = new Import();
        $import
            ->setFile($importLine->getFilename())
            ->setData($importLine->getData())
            ->setLine($importLine->getLine())
            ->setGroup($importLine->getGroup())
        ;

        $this->dm->persist($import);
        $this->dm->flush();
    }
}
