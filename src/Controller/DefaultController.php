<?php

namespace App\Controller;

use App\Document\Import;
use App\Reader\CsvReader;
use App\Message\ImportLine;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig');
    }
    
    /**
     * @Route("/sync-import", name="sync_import")
     */
    public function syncImport(DocumentManager $dm): Response
    {
        $file = new CsvReader(sprintf('%s/uploads/hermes-upload.csv', $this->getParameter('kernel.project_dir')), ";");
        $file->setHasHeader(true);
        
        foreach ($file as $line => $row) {
            $import = new Import();
            $import
                ->setFile($file->getFilename())
                ->setData($row)
                ->setLine($line)
                ->setGroup($row['RISK_QUESTIONAIRE_ID'])
            ;

            $dm->persist($import);
            
            if ($line % 5) {
                $dm->flush();
            }
        }
        
        $dm->flush();

        $this->addFlash('success', 'File imported synchronously!');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/async-import", name="async_import")
     */
    public function asyncImport(MessageBusInterface $bus): Response
    {
        $file = new CsvReader(sprintf('%s/uploads/hermes-upload.csv', $this->getParameter('kernel.project_dir')), ";");
        $file->setHasHeader(true);
        
        foreach ($file as $line => $row) {
            $importLine = new ImportLine(
                $file->getFilename(),
                $line,
                $row['RISK_QUESTIONAIRE_ID'],
                $row
            );

            $bus->dispatch($importLine);
        }

        $this->addFlash('success', 'File imported asynchronously!');

        return $this->redirectToRoute('homepage');
    }
}
