<?php

namespace App\Message;

class ImportLine
{
    private $filename;
    private $line;
    private $group;
    private $data;
    
    
    public function __construct(
        string $filename,
        int $line,
        string $group,
        array $data
    ) {
        $this->filename = $filename;
        $this->line = $line;
        $this->group = $group;
        $this->data = $data;
    }

    /**
     * Get the value of data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get the value of group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Get the value of line
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Get the value of filename
     */
    public function getFilename()
    {
        return $this->filename;
    }
}
